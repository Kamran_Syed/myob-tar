 <!--Begining of Help Pop UP DIV-->
        
        <div id="ttfcn"><?php _e( 'Use this field to give your custom class a name. Example: myCoolCss', $this->plugin_slug ); ?></div>
        <div id="ttfcc"><?php _e( 'Use this field to write your custom css.', $this->plugin_slug ); ?></div>
        <div id="ttfbt" ><?php _e( 'Use this field to change the text of your submit button.<br />Example: Submit or Download Now', $this->plugin_slug ); ?></div>
        <div id="ttfbtc" ><?php _e( 'Use this field to change the color of the button text.<br />Example: #000000 white', $this->plugin_slug ); ?></div>
        <div id="ttfbbc" ><?php _e( 'Use this field to change the color of the button.<br />Example: #000000 white', $this->plugin_slug ); ?></div>
        <div id="ttfw" ><?php _e( 'Use this field to adjust the width of the form.<br />Example: 80% or 550px', $this->plugin_slug ); ?></div>
        <div id="ttfip" ><?php _e( 'Use this field to add padding to the inside of the form<br />Example: 25px', $this->plugin_slug ); ?></div>
        <div id="ttfbc" ><?php _e( 'Use this field to change the background color of the form.<br />Example: #000000 or transparent or white', $this->plugin_slug ); ?></div>
        <div id="ttftc" ><?php _e( 'Use this field to change the text (label) color of the form.<br />Example: #000000 or white', $this->plugin_slug ); ?></div>
        <div id="ttftiw"><?php _e( 'Use this field to adjust the width of the text input fields.<br />Example: 80% or 350px', $this->plugin_slug ); ?></div>
        <div id="ttftibc"><?php _e( 'Use this field to change the background color of the text inputs.<br />Example: #000000 or transparent or white', $this->plugin_slug ); ?></div>
        <div id="ttfmb"><?php _e( 'Use this field to adjust the margin at the bottom of the form<br />Example: 50px', $this->plugin_slug ); ?></div>
        <div id="ttfmt"><?php _e( 'Use this field to adjust the margin at the top of the form<br />Example: 50px', $this->plugin_slug ); ?></div>
        <div id="ttfmr"><?php _e( 'Use this field to adjust the margin to the right of the form<br />Example: 25px', $this->plugin_slug ); ?></div>
        <div id="ttfml"><?php _e( 'Use this field to adjust the margin to the left of the form<br />Example: 25px', $this->plugin_slug ); ?></div>
        

    <div id="downloadNameHelp"><?php _e( 'Give your download file a friendly name.', $this->plugin_slug ); ?><br />
        <?php _e( 'For example: Jens eBook', $this->plugin_slug ); ?><br /></div>
    <div id="landingPageHelp"><?php _e( 'Select the page that you intend to use for a landing page.', $this->plugin_slug ); ?><br />
        <?php _e( 'If you are not sure why you need to select a landing page,', $this->plugin_slug ); ?><br />
        <?php _e( 'please review the instructions by ', $this->plugin_slug ); ?><a href="http://support.doubleoptinfordownload.com/support/solutions/articles/5000526388-how-to-setup-your-first-download" target="new" ><?php _e( 'Clicking Here.', $this->plugin_slug ); ?></a><br />
        <?php _e( 'What is a landing page you ask? ', $this->plugin_slug ); ?><a href="http://www.doubleoptinfordownload.com/what-is-a-landing-page/" target="new"><?php _e( 'Click here to learn more about landing pages', $this->plugin_slug ); ?></a>.</div>
    <div id="selectDownloadHelp"><?php _e( 'Select the file that you want to offer as a free download.', $this->plugin_slug ); ?><br />
        <?php _e( 'If you are editing your download you will only need to select your download again', $this->plugin_slug ); ?><br />
        <?php _e( 'if you are changing it, other wise you can leave this empty when editing.', $this->plugin_slug ); ?></div>
    <div id="selectDownloadFormHelp"><?php _e( 'Select the form that you would like to use for this download', $this->plugin_slug ); ?><br />
        <?php _e( 'You can create a custom form just for this download by clicking', $this->plugin_slug ); ?><br />
        <?php _e( '"Create Form" to the right. You can change forms at any time.', $this->plugin_slug ); ?><br /></div>
    <div id="downloadFormNameHelp"><?php _e( 'Give your form a name so that you can easily identify it.', $this->plugin_slug ); ?></div>
    <div id="ttulfh" ><?php _e( 'Check this box to use the old legacy forms.', $this->plugin_slug ); ?><br>
        <p style="color: #ff0000;"><?php _e( 'Note: Legacy Forms will be discontinued in December 2016!', $this->plugin_slug ); ?></p></div>
    <div id="ttuflh" ><?php _e( 'Check this box if you want form field labes on your form.', $this->plugin_slug ); ?></div>
    <div id="ttuphh" ><?php _e( 'Check this box if you want placeholder in your text fields', $this->plugin_slug ); ?>
        <p style="color: #ff0000"><?php _e( 'Placeholders are not supported in all browsers.', $this->plugin_slug ); ?></p></div>
    <div id="ttfht" ><?php _e( 'Put your desired header text here. Example: My Aweseome Free Download', $this->plugin_slug ); ?></div>
    <div id="ttfhf" ><?php _e( 'Select your desired font for your header text', $this->plugin_slug ); ?></div>
    <div id="ttfhfs"><?php _e( 'Input your desired text size for the header font.<br />Example: 1.2em or 10px', $this->plugin_slug ); ?></div>
    <div id="ttfhtc"><?php _e( 'Select your desired color for your header font.', $this->plugin_slug ); ?></div>
    
    <div id="ttfdt" ><?php _e( 'Put your desired description text here.', $this->plugin_slug ); ?><br /><?php _e( 'Example: Provide your name and email address to receive my Awesome Download', $this->plugin_slug ); ?></div>
    <div id="ttfdf" ><?php _e( 'Select your desired font for your description text.', $this->plugin_slug ); ?></div>
    <div id="ttfdfs"><?php _e( 'Input your desired text size for the description font.', $this->plugin_slug ); ?><br /><?php _e( 'Example: 1.2em or 10px', $this->plugin_slug ); ?></div>
    <div id="ttfdtc"><?php _e( 'Select your desired color for your description font.', $this->plugin_slug ); ?></div>
    <div id="ttfborw"><?php _e( 'Use this control to set the width of your border.', $this->plugin_slug ); ?></div>
    <div id="ttfborc"><?php _e( 'Use this control to choose the color of the border.', $this->plugin_slug ); ?></div>
    <div id="ttfbors"><?php _e( 'Use this control to select the border style.', $this->plugin_slug ); ?></div>
    <div id="ttfborr"><?php _e( 'Use this control to set the form border radius.', $this->plugin_slug ); ?></div>
    <?php  if(  class_exists( 'DOIFDPremiumAdminUploadFormFields' )) {
        $helpMSG = new DOIFDPremiumAdminUploadFormFields();
        $helpMSG->helpMessages();
    }
    ?>
        
        <!--END of Help Pop UP DIV-->

