<?php

/**
 * Plugin Name: Tarnia Elite Myob Exo.
 * Plugin URI: 
 * Description: Tarnia Elite Myob Exo.
 * Version: 2.0
 * Author: Agile Solutions PK
 * Author URI: http://agilesolutionspk.com
 */
 
/*  error_reporting(E_ALL);
ini_set('display errors', 1);  */


require_once(__DIR__ ."/classes/order_details_model.php");
require_once(__DIR__ ."/classes/sku_details_model.php");

if ( !class_exists('TarniaeliteMyobExo')){
	
	class TarniaeliteMyobExo{
		
		function __construct(){
			
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('admin_menu', array(&$this,'admin_menu'));
			add_action( 'wp_ajax_aspk_supd', array(&$this, 'stock_update'));
			add_action( 'wp_ajax_nopriv_aspk_supd', array(&$this, 'stock_update') );
			add_action( 'wp_ajax_aspk_updskus', array(&$this, 'update_all_skus') );
			add_action( 'wp_ajax_nopriv_aspk_updskus', array(&$this,'update_all_skus') );
			//add_action( 'woocommerce_order_status_processing',array(&$this, 'order_status_changed'));
			add_shortcode( 'exo_customer_purchase_history', array(&$this,'exo_customer_purchase_history') );
			add_action('init', array(&$this, 'init'));
			add_action( 'wp_ajax_aspk_impcustomers', array(&$this,'import_customers') );
			add_action( 'wp_ajax_nopriv_aspk_impcustomers', array(&$this,'import_customers') );
			add_action( 'wp_ajax_aspk_newitem', array(&$this,'get_new_item') );
			add_action( 'wp_ajax_nopriv_aspk_newitem', array(&$this,'get_new_item') );

			
		}
		
		function init(){
			
			if(! session_id() ) session_start();
			
		}
		
		function install() { 
			
			$OrderDetailsModel = new OrderDetailsModel();
			$OrderDetailsModel->create_table();
			
			$skuDetailsModel = new SkuDetailsModel();
			$skuDetailsModel->create_table();
			
			
		}
		
		function admin_menu(){
			
			add_menu_page('Aspk Myob', 'Aspk Myob', 'manage_options', 'aspk_myob', array(&$this, 'aspk_myob_func'));
			
			
		}
		
		function aspk_myob_func(){
			
			if(isset($_POST['aspkSaveSettings'])) {
				
				$username = trim($_POST['username']);
				$password = trim($_POST['password']);
				$apiKey = trim($_POST['apiKey']);
				$apiHost = trim($_POST['apiHost']);
				$accessToken = trim($_POST['accessToken']);
				
				$authHash = "Basic ". base64_encode($username . ':' . $password);
				
				update_option( 'aspk_exo_username', $username );
				if($password != "12345678") update_option( 'aspk_exo_password', $authHash );
				update_option( 'aspk_exo_apiKey', $apiKey );
				update_option( 'aspk_exo_apiHost', $apiHost );
				update_option( 'aspk_exo_accessToken', $accessToken );
				
			}
			
			?>
				<form name="dontcare"  method="POST">
					<table style="width:40%">
						
							<th> <h3>Settings For My API Integration </h3> </th>
						
						<br>
						<tr>
							<td> Username: </td> 
							<td><input type="text" name="username" value= "<?php echo get_option( 'aspk_exo_username' ); ?>"></td>
						</tr>
						<?php if(get_option( 'aspk_exo_password')) $p= "12345678"; 


						?>
						<tr>
							<td> Password: </td> 
							<td> <input type="password" name="password" value= "<?php echo $p; ?>"></td>
						</tr>
						<tr>
							<td>Api-host: </td>
							<td><input type="text" name="apiHost" value= "<?php echo get_option( 'aspk_exo_apiHost' ); ?>"></td>
						</tr>
						<tr>
							<td>Api-key: </td> 
							<td><input type="text" name="apiKey" value= "<?php echo get_option( 'aspk_exo_apiKey' ); ?>"></td>
						</tr>
						<tr>
							<td>Access Token:</td> 
							<td> <input type="text" name="accessToken" value= "<?php echo get_option( 'aspk_exo_accessToken' ); ?>"></td>
						</tr>
						<tr>
							<td><button type="submit" name="aspkSaveSettings" class="btn btn-primary" >Save Settings</button></td>
						</tr>
					</table>
											
					
				</form>
			<?php
		} 
		
		function stock_update(){
			//stock and price update code goes here
			$skuList = $this->get_sku_list();
			
			if(!$skuList) {
				$msg = "no sku list found.";
				$this->aspk_log($msg);
				return;
			} 
			$authHash = get_option( 'aspk_exo_password' );
			$apiKey = get_option( 'aspk_exo_apiKey' );
			$accessToken = get_option( 'aspk_exo_accessToken' );

			foreach ($skuList as $k => $v){
				
				$sku = $v->sku;
				
				$host = "https://exo.api.myob.com/stockitem/".$sku;
							
				$response = $this->sendPost($host,$authHash,$apiKey,$accessToken);
			
				if($response){ 
				
					$this->process_stock_item_update($response,$sku, $authHash, $apiKey, $accessToken); 
		
				} 
				
			} 
			
			exit;
		}
		
		function get_sku_list(){
			
			$sdm = new SkuDetailsModel();
			$skuList = $sdm->get_sku_list_oldupdated();
			
			return $skuList;
		}
		
		function process_stock_item_update($response, $sku, $authHash, $apiKey, $accessToken){
			
			$price = "0";
			$qty = 0;
			$kv = array();
			$sdm = new SkuDetailsModel();
			$prodID = wc_get_product_id_by_sku( $sku );
			$prod = wc_get_product( $prodID );
			
			$status = $response->active;
			if($status == true){
				$prod->set_catalog_visibility('visible');
				
			}else{
				$prod->set_catalog_visibility('hidden');
				$prod->save();
				return;
			}
				
			$qty = $response->totalinstock;
			$salePrices = $response->saleprices;
			$priceCard = json_encode($salePrices); 
			
			foreach ($salePrices as $k => $v){
			
				$id = $v->id;
				if($id == "1") {
					$price = (string) round($v->price, 2);
					break;
				}
			}
			

			if($qty >= 0) {
				
				//if($prod->get_name != $response->description){
				if(true){ //TODO

					$host = "https://exo.api.myob.com/stockitem/".$sku."/image?height=auto&width=auto";
					
					try{

						$img = $this->getImage($host,$authHash,$apiKey,$accessToken);
						if($img !== false){

							$this->deleteProductImages($prodID);
							$this->saveProductImage($img, $prodID, $response->description);

						}

					}catch(Exception $ex){
						$this->aspk_log($ex->getMessage());
					}
					

				}

				$prod->set_manage_stock(true);
				$prod->set_stock_quantity($qty);
				$prod->set_sale_price($price);
				$prod->set_name($response->description);
				$prod->set_description($response->description);
				$prod->save();

				$kv['qty']= $qty;
				$kv['price']= $priceCard;
				
				$sdm->update_item($sku,$kv);
				
			} 
				
					
		}
		
		function update_all_skus() {
			global $wpdb;
			$kv = array();
			
			$dbSkucount = $wpdb->get_var("SELECT count(pm.meta_value) FROM wp_posts as p, wp_postmeta as pm WHERE p.post_type = 'product' and p.post_status = 'publish' and pm.meta_key='_sku' and p.ID = pm.post_id");
			
			$countSku = intval(get_option('aspk_count_sku'));
			$sdm = new SkuDetailsModel();
	
			$dbSkus = $sdm->get_all_skus();

			if($dbSkucount != $countSku) {
				
				$sql = "SELECT pm.meta_value, p.ID FROM wp_posts as p, wp_postmeta as pm WHERE p.post_type = 'product' and p.post_status = 'publish' and pm.meta_key='_sku' and p.ID = pm.post_id";
				$skus = $wpdb->get_col($sql);
				
				$diff = array_diff($skus, $dbSkus);
			
				 
				foreach($diff as $sku) {
				
					$kv['price']= 0;
					$kv['qty']= 0;

					if(in_array($sku, $dbSkus)) $sdm->delete_item($sku);

					if(in_array($sku, $skus)) $sdm->add_item($sku , $kv);
				
				}
				
				$count = $wpdb->get_var("SELECT count(pm.meta_value) FROM wp_posts as p, wp_postmeta as pm WHERE p.post_type = 'product' and p.post_status = 'publish' and pm.meta_key='_sku' and p.ID = pm.post_id");
			
				update_option('aspk_count_sku', $count);
			}
		
			
			exit;
		}
		
		function aspk_log($err){
			file_put_contents(ABSPATH.'/wp-content/uploads/aspk_exo_error_log', __FILE__ .' '. __LINE__ .' '. print_r($err, true) . PHP_EOL, FILE_APPEND);
		}
		
		function sendPost($host,$authHash,$apiKey,$accessToken, $kv=array()){

			$headers[] = 'Authorization: '.$authHash;
			$headers[] = 'Accept: application/json';
			$headers[] = 'x-myobapi-key: '.$apiKey;
			$headers[] = 'x-myobapi-exotoken: '.$accessToken;
			
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $host);
			if(! empty($kv)){ //GET
				
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $kv ));
			}
			
			curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec ($ch);

			if($server_output === false){
				
				print_r(curl_error($ch));
				return false;
				
			}
			curl_close ($ch);
			
			return json_decode($server_output);
		}

		function getImage($host,$authHash,$apiKey,$accessToken, $kv=array()){

			$headers[] = 'Authorization: '.$authHash;
			$headers[] = 'Accept: image/jpeg';
			$headers[] = 'x-myobapi-key: '.$apiKey;
			$headers[] = 'x-myobapi-exotoken: '.$accessToken;
			
			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $host);
			if(! empty($kv)){ //GET
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query( $kv ));
			}
			
			curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$server_output = curl_exec ($ch);

			if($server_output === false){
				
				print_r(curl_error($ch));
				return false;
				
			}

			$imgSize = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);

			curl_close ($ch);
			if($imgSize < 2000) return false;

			return $server_output;
		}
		
		function saveProductImage(&$image, $product_id, $title){
			$upload_dir = wp_upload_dir();
			$filename = $upload_dir['path'].'/'. sanitize_file_name( $title .$product_id ).'.jpg';

			file_put_contents($filename, $image);

			// Check the type of file. We'll use this as the 'post_mime_type'.
			$filetype = wp_check_filetype( basename( $filename ), null );

			// Prepare an array of post data for the attachment.
			$attachment = array(
				'guid'           => $upload_dir['url'] . '/' . basename( $filename ), 
				'post_mime_type' => $filetype['type'],
				'post_title'     => sanitize_title(basename( $filename )),
				'post_content'   => '',
				'post_status'    => 'inherit'
			);

			// Insert the attachment.
			$attach_id = wp_insert_attachment( $attachment, $filename, $product_id );

			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );

			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );

			set_post_thumbnail( $product_id, $attach_id );

			update_post_meta( $product_id, '_thumbnail_id', $attach_id );
			
		}


		function deleteProductImages($product_id){

			/*$args = array(
                'post_type' => 'attachment',
                'posts_per_page' => -1,
                'post_status' => 'any',
                'post_parent' => $product_id
            );

            $attachments = get_posts($args);

            if(isset($attachments) && is_array($attachments)){
                foreach($attachments as $attachment){
                    
                    wp_delete_attachment( $attachment->ID );
                }
            }*/

            delete_post_thumbnail( $product_id );

		}


		
		function order_status_changed($order_id ){
			global $woocommerce;
			
			$order = new WC_Order($order_id);
			$total = $order->get_total();
			$user_id = $order->user_id;
			$items = $order->get_items();
			
			$user_email = get_user_meta( $user_id, 'shipping_email', true );
			$user_zip = get_user_meta( $user_id, 'shipping_postcode', true );
			$user_phone = get_user_meta( $user_id, 'shipping_phone', true );
			$company_name = get_user_meta( $user_id, 'shipping_company', true );
			
			$deliveryaddress = array();
			$deliveryaddress['line1'] = get_user_meta( $user_id, 'shipping_address_1', true );
			$deliveryaddress['line2'] = get_user_meta( $user_id, 'shipping_address_2', true );
			$deliveryaddress['line3'] = get_user_meta( $user_id, 'shipping_city', true );
			$deliveryaddress['line4'] = get_user_meta( $user_id, 'shipping_state', true );
			$deliveryaddress['line5'] = get_user_meta( $user_id, 'shipping_country', true );
			$deliveryaddress['line6'] =	'';
				
			$api_order_id = $this->create_order_via_api($user_id, $order_id, $items, $total, $deliveryaddress);
			
		}
		
		function create_order_via_api($debtorid, $order_id, $items, $total, $deliveryaddress){
			global $woocommerce;
			
			$line_items = array();
			
			foreach($items as $item){
				$line_item = array();
				
				$qty = $item['qty'];
				$product_id = $item['product_id'];
				$prod = new WC_Product($product_id);
				if($prod->is_virtual()){
					$stocktype = "LookupItem";
				}else{
					$stocktype = "PhysicalItem";
				}
				
				$line_item['stockcode'] = $prod->get_sku();
				$line_item['orderquantity'] = $qty;
				$line_item['unitprice'] = round( ($item['line_total'] / $item['qty'] ), 2);
				$line_item['taxratevalue'] = $item['line_tax'];
				$line_item['linetotal'] = $item['line_total'];
				$line_item['linetype'] = 0;
				$line_item['description'] = $prod->get_title();
				$line_item['stocktype'] = $stocktype;
				$line_item['duedate'] = date('Y-m-d');
				$line_item['IsOriginatedFromTemplate'] = 'false';
				$line_item['discount'] = 0;
				$line_item['IsPriceOverridden'] = 'false';
				$line_item['LocationId'] = 1;
				$line_item['TaxRateId'] = 10; //problem here TODO
				$line_item['listprice'] = $line_item['unitprice'];
				$line_item['pricepolicyid'] = -1;
				$line_item['narrative'] = '';
				$line_item['BatchCode'] = '';
				//$line_item['BranchId'] = 0;
				
				$line_items[] = $line_item;
				
			}
			
			$order = new WC_Order($order_id);
			
			$user_uri = get_option( 'aspk_exo_apiHost');
			$url = $user_uri.'/salesorder/';
			
			$user_auth_name = get_option( 'aspk_exo_password');
			$user_api_key = get_option( 'aspk_exo_apiKey');
			$user_acces_token = get_option( 'aspk_exo_accessToken');
			
			$headers = array();
			$authorization = $user_auth_name;
			$headers['Authorization'] = $authorization;
			$headers['x-myobapi-key'] = $user_api_key;
			$headers['x-myobapi-exotoken'] = $user_acces_token;
			$headers['Accept'] = 'application/json';
			$headers['Content-Type'] = 'application/json';
			
			$body = array(
				'debtorid' => $debtorid,
				'lines' => $line_items,
				'status' => 0,
				'ordertotal' => $total,
				'narrative' => home_url(),
				'allowcustomerordernumber' => 'true',
				'reference' => '',
				'instructions' => '',
				'finalisationdate' => '',
				'activationdate' => '',
				'BranchId' => 0,
				'DefaultLocationId' => -1,
				'ContactId' => -1,
				'SalesPersonId' => 61,
				'ExchangeRate' => 1,
				'OrderDate' => date('Y-m-d'),
				'duedate' => date('Y-m-d'),
				'DeliveryAddress' => $deliveryaddress,
				'extrafields' => '',
				'customerordernumber' => $order_id,
				'id' => 0,
				'currencyid' => 0
				
			);
						
			$args = array(
				'method' => 'POST',
				'timeout' => 45,
				'redirection' => 5,
				'httpversion' => '1.0',
				'blocking' => true,
				'headers' => $headers,
				'body' => json_encode($body),
				'cookies' => array()
			);
			
			
			
			$response = wp_remote_post( $url, $args);
			
			
			
			$body = $response['body'];
			$body_obj = json_decode($body);
			
			
			
			if ( is_wp_error( $response ) ) {
			   $error_message = $response->get_error_message();
			   $order->add_order_note("Error:".$error_message);
			   return '0';
			   
			}else{
				$ret = $response['response'];
				if($ret['code'] != '201'){
					$order->add_order_note("Error:".$ret['message'] .$body_obj->message  );
					return '0';
				}
				
				$order->add_order_note("Order created on EXO with id ".$body_obj->id);
				
				update_post_meta( $order_id, '_dropbox_order_id', $body_obj->id);
			}
			
		}
		
		
		
		function get_user_from_exo($user){
			
			
			$user_uri = get_option( 'aspk_exo_apiHost');
			$host = $user_uri.'/debtor/'.$user;
							
							
			$authHash = get_option( 'aspk_exo_password' );
								
			$apiKey = get_option( 'aspk_exo_apiKey' );
								
			$accessToken = get_option( 'aspk_exo_accessToken' );
								
			$response = $this->sendPost($host,$authHash,$apiKey,$accessToken);
							
			
			if($response){
				
					$id = $response->id;
						
					if( $user == $id ){
						return $id;
					}
				
				return '0';
				
			}
			
		}
		
		function exo_customer_purchase_history(){
			
			$date =0;
			$invoice =0;
			$price =0;
			$item =0;
			$qty =0;
			$lines = 0;
			
			$response = 0;
								
				if(isset($_SESSION['exo_purchase_history'])){
					
					$response = json_decode($_SESSION['exo_purchase_history']);
					
				}else{
					
					
					$current_user = wp_get_current_user();
						 if($current_user){
	   
							$username = $current_user->user_login ;
							$user_email = $current_user->user_email;
							$user_first_name = $current_user->user_firstname;
							$user_last_name = $current_user->user_lastname;
							$user_ID = $current_user->ID;
						 }
						
						 		
						$api_user_id = $this->get_user_from_exo($username);
									
						if($api_user_id == '0'){
							
							return ; //customer does not exist
						
						}else{
							 //Customer exists on EXO with id $api_user_id
							
							$user_uri = get_option( 'aspk_exo_apiHost');
							$host = $user_uri.'/debtor/'.$api_user_id.'/transaction';
							
							
							$authHash = get_option( 'aspk_exo_password' );
								
							$apiKey = get_option( 'aspk_exo_apiKey' );
								
							$accessToken = get_option( 'aspk_exo_accessToken' );
								
							$response = $this->sendPost($host,$authHash,$apiKey,$accessToken);
						
							
							if($response){ 
								
								$json_array = json_encode($response); 
								$_SESSION['exo_purchase_history'] = $json_array;
								
							}
						}
				}
										
				
					if($response){ 
							
									?>
										<!-- <div class="featured-box align-left">
											<div class="box-content"> -->
												<table class="woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
													<caption> <h3> Order History </h3> </caption>
													<thead>
														<tr>
													
															<th class="invoice-number"><span class="nobr">Invoice Number</span></th>
															<th class="transaction-date"><span class="nobr">Transaction Date</span></th>
															<th class="item"><span class="nobr">Item</span></th>
															<th class="item_price"><span class="nobr">Price(+tax)</span></th>
															<th class="qty"><span class="nobr">Quantity</span></th>
														</tr>
													</thead>
													<tbody>
													<?php 
												foreach($response as $k=>$v){
									
														$date = $v->transactiondate;
														$invoice = $v->invoicenumber;
														
														$lines = $v->lines;
														
														if($lines){
															foreach($lines as $k=>$v){
																$item = $v->description;
																$qty = $v->quantity;
																$price = $v->totalincludingtax;
																
																
																if($item){
																	?>
																		<tr class="order">
																			
																			  <td class="invoice-number" data-title="Invoice Number"> <?php echo $invoice; ?> </td>

																			  <td class="transaction-date" data-title="Transaction Date"><?php 
																			  
																			  $timestamp = strtotime($date);
																			  echo date('d/m/Y', $timestamp );
															
																			  ?> </td>

																			   <td class="item" data-title="Item"><?php echo $item;?></td>
																			
																			  <td class="item_price" data-title="Price"> $<?php  echo $price; ?> </td>

																			   <td class="qty" data-title="Quantity"><?php echo $qty;?></td> 

																		</tr>
															  <?php }
															
															} 
														}
													
												 } ?>
														  
													</tbody>		   
												</table>
											<!--</div>
										</div>-->
											
											
											<?php
										
									
									
								}
				
		}
	
		function import_customers(){
			
			$pg = get_option( 'aspk_tarnia_page' );
			$page_no = intval($pg);
		
			$user_uri = get_option( 'aspk_exo_apiHost');
			$host = $user_uri.'/debtor?page='.$page_no.'&pagesize=25';
							
			$authHash = get_option( 'aspk_exo_password' );
								
			$apiKey = get_option( 'aspk_exo_apiKey' );
								
			$accessToken = get_option( 'aspk_exo_accessToken' );
								
			$response = $this->sendPost($host,$authHash,$apiKey,$accessToken);
			
			if($response){
				
				foreach($response as $k=>$v){
				
					$email = $v->email;
					$id =  $v->id;
					
					$pass = rand(10000, 99000);
					
					if($email){
						
						$userdata = array(
							'user_login'  =>  $id,
							'user_email'    =>  $email,
							'user_pass'   =>  $pass, 
							'role'   =>  "customer", 
						);
						
						$user_id = wp_insert_user( $userdata ) ;
							
						//On failure
						if ( is_wp_error( $user_id ) ) {
							
							continue;
						}
						
					}
				}
				
				$count = count($response);
				
				if($count != 25) {
					$page =0;
				}
				else {
					$page = $page_no + 1;
					
				}
				
				update_option( 'aspk_tarnia_page', $page );
				
			}
			exit;//being ajax call
		} 
		
		
		function get_new_item(){
			
			$pg = get_option( 'aspk_tarnia_page_no' );
			$page_no = intval($pg);
			
			$host = 'https://exo.api.myob.com/stockitem?page='.$page_no.'&pagesize=100';
							
			$authHash = get_option( 'aspk_exo_password' );
								
			$apiKey = get_option( 'aspk_exo_apiKey' );
								
			$accessToken = get_option( 'aspk_exo_accessToken' );
								
			$response = $this->sendPost($host,$authHash,$apiKey,$accessToken);
			
			$price = "0";
			$qty = 0;
			$kv = array();
			$sdm = new SkuDetailsModel();
			
			$skuList = $this->get_sku_list();
			
			if(!$skuList) {
				$msg = "no sku list found.";
				$this->aspk_log($msg);
				return;
			} 
			
			
			if($response){
				
				foreach($response as $k=>$v){
					$sku =  $v->id;
					
					$host2 =  "https://exo.api.myob.com/stockitem/".$sku;
					
					$response2 = $this->sendPost($host2,$authHash,$apiKey,$accessToken);
					
					foreach ($skuList as $k => $v){
				
						$sku_table = $v->sku;
						
						if($sku == $sku_table) continue;
						
						else{
								
							$qty = $response2->totalinstock;
							$salePrices = $response2->saleprices;
							$priceCard = json_encode($salePrices); 
							
							$kv['qty']= $qty;
							$kv['price']= $priceCard;
							
							$sdm->update_get_newitem($sku,$kv);
							
						} 
							
						
					}
					
				}
				
				$count = count($response);
				
				if($count != 100) {
					$page =0;
				}
				else {
					$page = $page_no + 1;
					
				}
				
				update_option( 'aspk_tarnia_page_no', $page );
				
				
			}
			
		}
		
		
	}// class ends
	
}// if ends

new TarniaeliteMyobExo();