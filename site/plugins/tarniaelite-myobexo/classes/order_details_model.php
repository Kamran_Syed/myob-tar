<?php
if ( !class_exists( 'OrderDetailsModel' )){

	class OrderDetailsModel{
		private $tbl = "order_details";
		
		function __construct(){
			
		}
		
		function create_table(){
			global $wpdb;
			
			$table_name = $wpdb->prefix . 'order_details';
			$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				  `sku` varchar(30) NOT NULL PRIMARY KEY,
				  `qty` decimal(10,0) DEFAULT NULL,
				  `description` text,
				  `price` decimal(10,0) NOT NULL,
				  `promo` varchar(3) DEFAULT NULL,
				  `discount(%)` decimal(10,0) DEFAULT NULL,
				  `total` decimal(10,0) DEFAULT NULL,
				  `batchcode` varchar(30) DEFAULT NULL,
				 `GST` varchar(30) DEFAULT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
				
					
				$wpdb->query($sql);
			
			
		}
		
		
		
	} //class ends
	
} //if class ends