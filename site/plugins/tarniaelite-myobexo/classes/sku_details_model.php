<?php
if ( !class_exists( 'SkuDetailsModel' )){

	class SkuDetailsModel{
		private $tbl = "sku_details";
		
		function __construct(){
			
		}
		
		function create_table(){
			global $wpdb;
			
			$table_name = $wpdb->prefix . 'sku_details';
			$sql = "CREATE TABLE IF NOT EXISTS ".$table_name."(
				  `sku` varchar(30) NOT NULL PRIMARY KEY,
				  `qty` int(4) DEFAULT NULL,
				  `price` text NOT NULL,
				  `description` text,
				  `lastupdated` timestamp NULL DEFAULT NULL,
				  `soldcount` int(5) DEFAULT NULL
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
				
				

			$wpdb->query($sql);
		
			
		}
		
		function add_item($sku, $kv){
			global $wpdb;
			
			$lastupdated = time();
			
			if(empty($sku)) throw new Exception('Cannot Add Item. SKU must be present');
			
			$item = $this->get_item($sku);
			if($item) throw new Exception('SKU '.$sku.' already exists.');
			
			$sql = "INSERT INTO {$wpdb->prefix}sku_details ( `qty`, `price`, `lastupdated`, `sku`)
			VALUES('{$kv['qty']}', '{$kv['price']}', '{$lastupdated}', '{$sku}')";
			$wpdb->query($sql);
			
			return "success";
	
		}
		
		
		function get_item($sku){
			global $wpdb;
			
			if(empty($sku)) return false;
			$sql = "SELECT * FROM ".$wpdb->prefix."sku_details WHERE sku = '".$sku."'";
			
			$ret = $wpdb->get_row($sql);
			
			return $ret;
			
		}
		
		function update_item($sku, $kv){
			global $wpdb;
			
			$lastupdated = time();
			
			$item = $this->get_item($sku);
			if(!$item) $this->add_item($sku, $kv);
			
			$sql = "UPDATE {$wpdb->prefix}sku_details SET  qty= '{$kv['qty']}', price= '{$kv['price']}', lastupdated= '{$lastupdated}'	WHERE `sku`= '{$sku}'";
			$wpdb->query($sql);
			
			return "success";
			
		}
		
		function update_get_newitem($sku, $kv){
			global $wpdb;
			
			$lastupdated = "631152000";
			
			$item = $this->get_item($sku);
			if(!$item) $this->add_item($sku, $kv);
			
			$sql = "UPDATE {$wpdb->prefix}sku_details SET  qty= '{$kv['qty']}', price= '{$kv['price']}', lastupdated= '{$lastupdated}'	WHERE `sku`= '{$sku}'";
			$wpdb->query($sql);
			
			return "success";
			
		}
		
		function delete_item($sku){
			global $wpdb;
			
			$item = $this->get_item($sku);
			if(!$item) throw new Exception('sku '.$sku.' does not exist.');
			
			$sql = "DELETE FROM {$wpdb->prefix}sku_details
			WHERE `sku`= '{$sku}'";
			$wpdb->query($sql);
			
			return "success";
			
		}
		
		function get_sku_list_oldupdated($num = 500){
			
			global $wpdb;
			
			$sql = "SELECT * FROM ".$wpdb->prefix."sku_details ORDER BY lastupdated ASC limit ".$num."";
			
			$ret = $wpdb->get_results($sql);
			
			return $ret;
		}
		
		function get_all_skus() {
			
			global $wpdb;
			
			$sql = "SELECT * FROM ".$wpdb->prefix."sku_details";
			
			$ret = $wpdb->get_results($sql);
			
			return $ret;
			
		}
		
	} //class ends
	
} //if class ends